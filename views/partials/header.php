<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title>8pm</title>
    <link rel="stylesheet" href="src/css/reset.css"> 
    <link rel="stylesheet" href="src/css/bootstrap.min.css">   
    <link rel="stylesheet" href="src/css/inscription.css">
    <link rel="stylesheet" href="src/css/connexion.css">
    <link rel="stylesheet" href="src/css/style.css">
    <link rel="stylesheet" href="src/css/selection.css">
    <link rel="stylesheet" href="src/css/404.css">
    <link rel="stylesheet" href="src/css/home.css">
    <link rel="stylesheet" href="src/css/movie_suggestions.css">
    <link rel="stylesheet" href="src/css/footer.css">
    <link rel="stylesheet" href="src/css/legal_notices.css">

  </head>

    <body class="<?= $class ?>">  
        <header>
            <!-- navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- toggle for mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapse" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"></a>
                    </div>
                    <!-- classic nav -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <li><a class="navbar-brand" href="#"><img class="logo" class="navbar-brand" src="src/images/logo.png"></a></li>
                        <li><a href="home">Home</a></li>
                        <li><a href="movies_suggestions">Suggestions</a></li>
                      </ul>
                      <ul class="nav navbar-nav navbar-right">
                    <?php if(!empty($_SESSION['user'])): ?>
                        <li class="salutation"><a href="connexion"><span class="glyphicon glyphicon-user"></span>My account</a></li>
                        <li><a href="disconnect">Disconnect</a></li>
                    <?php else: ?>
                        <li><a href="inscription"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="connexion"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                      </ul>
                    <?php endif; ?>
                    </div>  
                </div>
            </nav>
        </header>