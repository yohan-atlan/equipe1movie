	<footer>
    <div class="row">
      <div class="col-xs-4  text-center">
          <ul>
              <li><a href="legal_notices">Legal notices</a></li>
          </ul>
      </div>
      <div class="col-xs-4 text-center moovie">
          <p class="movie">This website use Movie DB Api</p>
      </div>
      <div class="col-xs-4 text-center">
          <img class="moviedb" src="src/images/moviedb.png"/>
      </div>      
    </div>          
  </footer>

  <script src="src/js/libs/jquery.js"></script>
  <script src="src/js/libs/bootstrap.min.js"></script>
  <script src="src/js/app/script.js"></script>
</body>
</html>

