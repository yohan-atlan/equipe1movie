<title>Legal Notices</title>
<div>
        <h1>Legal Notices</h1>
        <h2>Registered Owner</h2>
        <p>This website is owned and maintained by Ronan Fourreau Cie., registered office address: 113 rue du Paris, Montreuil, France.</p>
        <h2>Conditions of Use</h2>
        <p>This page (together with the documents referred to on it) tells you the conditions of use (Conditions of use) on which you may make use of our website http://lamusejonction.com/movie_api/ (referred to in these Conditions of use as the Website), whether as a guest or a registered user. Please read these Conditions of use carefully before you start to use the Website. By using the Website, you indicate that you accept these Conditions of use and that you agree to abide by them. If you do not agree to these Conditions of use, please refrain from using the Website.</p>
        <h2>Accessing the website</h2>
        <p>Access to the Website is permitted on a temporary basis, and we reserve the right to withdraw or amend the service we provide on the Website without notice (see below). We will not be liable if for any reason the Website is unavailable at any time or for any period. From time to time, we may restrict access to some parts or all of the Website, to users who have registered with us.
        If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential, and you must not disclose it to any third party. We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our opinion you have failed to comply with any of the provisions of these Conditions of use.
        When using the Website, you must comply with the provisions of our acceptable use policy.
        You are responsible for making all arrangements necessary for you to have access to the Website. You are also responsible for ensuring that all persons who access the Website through your internet connection are aware of these Conditions of use, and that they comply with them.</p>
        <h2>Prohibited uses</h2>
        <p>You may use the Website only for lawful purposes. You may not use the Website: </p>
        <ul>
            <li>in any way that breaches any applicable local, national or international law or regulation</li>
            <li>in any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect</li>
            <li>for the purpose of harming or attempting to harm minors in any way</li>
            <li>to send, knowingly receive, upload, download, use or re-use any material which does not comply with our content standards to transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam); or</li>
            <li>to knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of any computer software or hardware.</li>
        </ul>
        <p>You also agree:</p>
        <ul>
            <li>not to reproduce, duplicate, copy or re-sell any part of the Website in contravention of the provisions of our Conditions of use, or</li>
            <li>not to access without authority, interfere with, damage or disrupt any part of the Website, any equipment or network on which the Website is stored any software used in the provision of the Website, or any equipment or network or software owned or used by any third party.</li>
        </ul>

    </div>
</div>

