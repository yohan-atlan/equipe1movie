    <h1>Welcome</h1>
    <h2>Sign-up so we can optimize the movie suggestions</h2>
    <h3>SIGN UP</h3>

    <div class="row">
        <div class="errors row">
        <?php if (!empty($errors)) : ?>
                    <div class="errors col-sm-4 col-sm-push-4"> 
                        <?php foreach ($errors as $_error) : ?>
                        <div> <?= $_error ?> </div>
                        <?php endforeach; ?>  
                    </div>
                <?php endif; ?>

                 <?php if (!empty($success)) : ?>
                    <div class="success"> 
                        <?php foreach ($success as $_success) : ?>
                        <div> <?= $_success ?> </div>
                        <?php endforeach; ?>  
                    </div>
                <?php endif; ?>
        </div>


        <div class="login row">
           <form class="col-xs-4 col-xs-offset-4 form-horizontal" role="form" action="#" method="POST">
               <div class="form-group username">
                    <label for="username">Username</label>
                    <input class="form-control form" type="text" id="username" name="username" value="<?= $username ?>" placeholder="username">
        
               </div>
               <div class="form-group email">
                    <label for="email">Email</label>
                    <input class="form-control form" type="email" id="email" name="email" value="<?= $email ?>" placeholder="myemail@mail.com">
               </div>
               <div class="form-group password">
                    <label for="password">Password</label>
                    <input class="form-control form" type="password" id="password" name="password" placeholder="password">
                </div>
                <div class="form-group password2">
                    <label for="password2">Password Verification</label>
                    <input class="form-control form" type="password" id="password2" name="password2" placeholder="password">
                </div>
              
                <div class="submit">
                    <label><input class="value_submit" type="submit" value="Submit"></label>
                </div>
               
           </form>
        </div>

        <div class="rectangle col-xs-8 col-xs-offset-2"></div>

        <div class="cont">
          <a href="connexion" class="<col-md-4 col-md-offset-4 col-xs-4 col-xs-offset-4 submit">Sign in</a>
        </div>

        <div class="continue">
          <a href="test_horsco.php" >go through anyway</a>
        </div>