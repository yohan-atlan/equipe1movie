<h2 class="col-md-4 col-md-offset-4 col-xs-4 col-xs-offset-4">Let's get started</h2>


<div class="container start">    
  <form action="selection" method="GET">
    <div class="row">

      <div class="col-sm-4 situation">
        <p>You are ...</p>
        <div class="select">  
          <input type="radio" class="option-input radio" name="situation" value="1">
          <label>With Friends</label>
        </div>
         <div class="select">  
          <input type="radio" class="option-input radio" name="situation" value="2">
          <label class="lab">With a Date</label>
        </div>
        <div class="select">  
          <input type="radio" class="option-input radio" name="situation" value="3">
          <label>Alone</label>
        </div> 
      </div>

      <div class="col-sm-4 mood">
      <p>You feel ...</p>
        <div class="select">  
          <input type="radio" class="option-input radio" name="mood" value="1">
          <label>Agitated</label>
        </div> 
        <div class="select">  
          <input type="radio" class="option-input radio" name="mood" value="2">
          <label>Curious</label>
        </div> 
        <div class="select">  
          <input type="radio" class="option-input radio" name="mood" value="3">
          <label>Quiet</label>
        </div> 
      </div>

      <div class="col-sm-4 caractere">
      <p>You are ...</p>
        <div class="select">  
          <input type="radio" class="option-input radio" name="caractere" value="1">
          <label>Freak</label>
        </div> 
        <div class="select">  
          <input type="radio" class="option-input radio" name="caractere" value="2">
          <label>Bad Guy</label>
        </div> 
        <div class="select">  
          <input type="radio" class="option-input radio" name="caractere" value="3">
          <label>Adventurer</label>
        </div>
      </div>

    </div>

    <!-- <div class="cont">
        <a href="inscription" class="inscription"><h5>Suggest me a movie</h5></a>
    </div> -->
    <div class="submit">        
      <input type="submit" value="Search" style="height: 35px;">
    </div>

  </form>
</div>





