$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});


/*
$(document).ready(function() {  
$('a[href^="#"]').click(function() {  
var target = $(this.hash);  
if (target.length == 0) target = $('a[name="' + this.hash.substr(1) + '"]');  
if (target.length == 0) target = $('html');  
$('html, body').animate({ scrollTop: target.offset().top }, 500);  
return false;  
});  
});
*/