<?php
session_start();
//Set variables
DEFINE('DB_HOST','localhost');

DEFINE('DB_NAME','Movie_API');

DEFINE('SALT','xc7wlopKx?lo');
DEFINE('DB_USER','root');

DEFINE('DB_PASS','root');

	
try
{
    //Try to connect to database
    $pdo = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASS);
    //Display accents
    $pdo->exec("SET CHARACTER SET utf8");
    //Set fetch mode to object
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
}
catch (Exception $e)
{
    //Failed to connect
    die('Cound not connect');
}

//else{
//	echo "connection ok";
//}
