<?php

// Config 
include 'config/options.php';
include 'config/database.php'; // Uncomment if you need database

// Get the query
$q = empty($_GET['q']) ? '' : $_GET['q'];

// Routes
if($q == '')
	$page = 'home';
else if($q == 'home')
	$page = 'home';
else if($q == 'selection')
	$page = 'selection';
else if($q == 'inscription')
	$page = 'inscription';
else if($q == 'connexion')
	$page = 'connexion';
else if($q == 'movies_suggestions')
	$page = 'movies_suggestions';
else if($q == 'disconnect')
	$page = 'disconnect';
else if($q == 'legal_notices')
	$page = 'legal_notices';
else
	$page = '404';

// Includes
include 'controllers/'.$page.'.php';
include 'views/partials/header.php';
include 'views/pages/'.$page.'.php';
include 'views/partials/footer.php';