<?php 
		if(!isset($_GET['situation']) OR !isset($_GET['mood']) OR !isset($_GET['caractere'])){
		header('Location:movies_suggestions');
		exit();
	}
	$title = 'Mon site';
	$class = 'selection';


	$code = $_GET['situation'].$_GET['mood'].$_GET['caractere'];

	$options = array(
		'111' => '878',
		'112' => '27',
		'113' => '12',
		'121' => '9648',
		'122' => '80',
		'123' => '10769',
		'131' => '14',
		'132' => '37',
		'133' => '99',
		'211' => '27',
		'212' => '53',
		'213' => '14',
		'221' => '36',
		'222' => '18',
		'223' => '10769',
		'231' => '80',
		'232' => '35',
		'233' => '10749',
		'311' => '10402',
		'312' => '10770',
		'313' => '10752',
		'321' => '36',
		'322' => '10752',
		'323' => '28',
		'331' => '10402',
		'332' => '35',
		'333' => '12'
	);


	$genre = $options[$code];
	$results = file_get_contents('http://api.themoviedb.org/3/genre/'.$genre.'/movies?api_key=bd21b457f2649eb45c40b5b944b9661c');

	$results = json_decode($results)->results;

	if(!empty($_SESSION['user'])){
		$id_users = $_SESSION['user']['id'];
		$id_movie = $_GET['id'];

		$query = $pdo->prepare("INSERT INTO views (id_movie,id_users) VALUES (:id_movie,:id_users) ");
	    $query->bindValue('id_movie',$id_movie);
	    $query->bindValue('id_users',$id_users);
	    $query->execute();
		$id_users = $_SESSION['user']['id'];

		$prepare = $pdo->prepare('SELECT id_movie FROM views WHERE id_users = :id_users');
	    $prepare->bindValue('id_users',$id_users);
	    $execute = $prepare->execute();
	    $seens  = $prepare->fetchAll();


	    foreach ($seens as $seen) {
	       foreach ($results as $key => $result) {
	            if ($result->id != $seen->id_movie) {
	            }
	          	else {
	            	unset($results[$key]);
	            }
	       }
	    }

	    $prepare = $pdo->prepare('SELECT * FROM views WHERE id_users = :id_users');
	    $prepare->bindValue('id_users',$id_users);
	    $execute = $prepare->execute();
	    $movies  = $prepare->fetchAll();

	    

	    $result_connexion = array();
	    foreach ($movies as $movie) {
	        $id = $movie->id_movie;
	        $api_query = file_get_contents('http://api.themoviedb.org/3/movie/'.$id.'?api_key=bd21b457f2649eb45c40b5b944b9661c');
	        $api_query = json_decode($api_query);
	        $result_connexion[] = $api_query;
	    }
	    $_SESSION['user']["result_connexion"] = $result_connexion;
	}

	