<?php
	
	$title = 'Sign In';
    $class = 'sign_in';
	

	$errors     = array();
    $success    = array();
	$email = ''; 
	$username = ''; 
	$password = ''; 
	$password2 = ''; 

	if (!empty($_POST)) {

		$email        = $_POST['email'];
		$username     = $_POST['username'];
		$password     = $_POST['password'];
		$password2    = $_POST['password2'];


		if (empty($username)) {
			$errors['username'] = 'Missing username';
			$password = '';
			$password2 = '';
		}

		if (empty($email)) {
			$errors['email'] = 'Missing email';
			$password = '';
			$password2 = '';
		}
		if (empty($password)) {
			$errors['password'] = 'Missing password';
			$password = '';
			$password2 = '';
		}
		else if ($password != $password2) {
            $errors['password'] = 'Not the same password';
            $password = '';
            $password2 = '';
        }

		$query = $pdo->prepare("SELECT * FROM users WHERE username = :username");

        $query->bindValue('username',$username);
        $query->execute();

        if($query->rowCount() > 0) {
        	$errors['username'] = 'Username already taken';
        } else {
        	if ($username && $email && $password && $password2) {
			
			if($password == $password2) {

				$password = hash('sha256', SALT.$password);
				$pdo->exec("INSERT INTO users VALUES ('', '$username', '$email', '$password')");
				header('Location: connexion');
				exit;
			}

			}
        } 
    }
