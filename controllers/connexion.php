<?php
    
    $title = 'Login';
    $class = 'login';


    if(!empty($_POST)){
        $username     = $_POST['username'];
        $password     = hash('sha256',SALT.$_POST['password']);

        $prepare = $pdo->prepare('SELECT * FROM users WHERE username = :username LIMIT 1');

        $prepare->bindValue('username',$username);
        $execute = $prepare->execute();
        $users    = $prepare->fetch();

        if ($users) {
            if ($users->mdp == $password) {
                $_SESSION['user'] = array(
                    'id'       => $users->id,
                    'username' => $users->username,
                );

                $id_users = $_SESSION['user']['id'];

                $prepare = $pdo->prepare('SELECT id_movie FROM views WHERE id_users = :id_users');
                $prepare->bindValue('id_users',$id_users);
                $execute = $prepare->execute();
                $movies  = $prepare->fetchAll();

                $result_connexion = array();
                foreach ($movies as $movie) {
                    $id = $movie->id_movie;
                    $api_query = file_get_contents('http://api.themoviedb.org/3/movie/'.$id.'?api_key=bd21b457f2649eb45c40b5b944b9661c');
                    $api_query = json_decode($api_query);
                    $result_connexion[] = $api_query;
                }
                $_SESSION['user']["result_connexion"] = $result_connexion;

            }
            else{
                echo "Login ou mot de passe incorrect";
            }
        }
        else{
            echo "Login ou mot de passe incorrect";
        }

    }





